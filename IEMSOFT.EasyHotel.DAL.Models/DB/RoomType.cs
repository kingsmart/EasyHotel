﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL.Models
{
    [PetaPoco.TableName("RoomType")]
    [PetaPoco.PrimaryKey("RoomTypeId", autoIncrement = true)]
   public class RoomType
    {
        public int RoomTypeId { get; set; }
        public string Name { get; set; }
        public decimal SinglePrice { get; set; }//房间单价
        public decimal MinFeeForHourRoom  { get; set; }//钟点房最低消费
        public decimal PricePerHour { get; set; }//钟点房每小时费用
        private DateTime _created = DateTime.Now;
        public DateTime Created { get { return _created; } set { _created = value; } }
        private DateTime _modified = DateTime.Now;
        public DateTime Modified { get { return _modified; } set { _modified = value; } }
        public int GroupHotelId { get; set; }
    }
}
