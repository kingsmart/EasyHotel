﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class RoomTypeModel
    {
        public string Name { get; set; }
        public decimal SinglePrice { get; set; }//房间单价
        public decimal MinFeeForHourRoom { get; set; }//钟点房最低消费
        public decimal PricePerHour { get; set; }//钟点房每小时费用
        public int RoomTypeId { get; set; }
    }
}